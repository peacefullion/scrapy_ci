
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app=Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"]='mysql+pymysql://root:123456@localhost/ci'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
#使用pymysql链接mysql数据库
db=SQLAlchemy(app)

class Pages(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    title=db.Column(db.Text)
    content=db.Column(db.Text)
    update=db.Column(db.DateTime)
    views=db.Column(db.Integer)

