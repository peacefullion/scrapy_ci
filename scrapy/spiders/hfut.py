import scrapy
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import datetime

app=Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"]='mysql+pymysql://root:123456@localhost/ci'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db=SQLAlchemy(app)
class Pages(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    url=db.Column(db.Text)
    title=db.Column(db.Text)
    content=db.Column(db.Text)
    update=db.Column(db.DateTime)
    views=db.Column(db.Integer)

class HfutSpider(scrapy.Spider):
    name = 'hfut'
    allowed_domains = ['ci.hfut.edu.cn']
    start_urls = ['http://ci.hfut.edu.cn/3961/list1.htm']

    def parse(self, response):
        for href in response.css('span.Article_Title a::attr(href)'):
            yield response.follow(href,self.parse_page)
        for href in response.css('a.next::attr(href)'):
            yield response.follow(href,self.parse)

    def parse_page(self,response):
        aurl=response.url
        atitle=response.css('title::text').extract_first()
        acontent=response.css('div.wp_articlecontent').extract_first()
        aupdate=response.css('span.arti_update::text').extract_first()
        aviews=response.css('span.WP_VisitCount::text').extract_first()
        apage=Pages(url=aurl,title=str(atitle),content=str(acontent),update=datetime.datetime.strptime(aupdate,'发布时间：%Y-%m-%d'),views=int(aviews))
        db.session.add(apage)
        db.session.commit()
